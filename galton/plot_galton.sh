for FILE in galton_*_*[^normalized].csv
do
n_bins=`echo $FILE | sed 's/galton_\([0-9]*\)_\([0-9]*\)\.csv/\1/g'`
n_balls=`echo $FILE | sed 's/galton_\([0-9]*\)_\([0-9]*\)\.csv/\2/g'`
gnuplot <<__EOF
set datafile separator ","
set term png
set output "galton_${n_bins}_${n_balls}.png"
set xlabel "bin index"
set ylabel "#balls"
set title "Ball distribution of a Galton board with ${n_bins} bins and ${n_balls} balls"
set autoscale x
plot "${FILE}" using 2 with histogram notitle
__EOF
done

for FILE in galton_*_*_normalized.csv
do
n_bins=`echo $FILE | sed 's/galton_\([0-9]*\)_\([0-9]*\)\_normalized.csv/\1/g'`
n_balls=`echo $FILE | sed 's/galton_\([0-9]*\)_\([0-9]*\)\_normalized.csv/\2/g'`
gnuplot <<__EOF
set datafile separator ","
set term png
set output "galton_${n_bins}_${n_balls}_normalized.png"
set xlabel "bin index"
set ylabel "frequency"
set title "Normalized ball distribution of a Galton board with ${n_bins} bins and ${n_balls} balls"
set autoscale x
plot "${FILE}" using 2 with histogram notitle
__EOF
done
