/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "model.h"
#include "Simulator.h"

#include <iostream>

#include <cereal/archives/binary.hpp>

using namespace cdevs;
using namespace cdevs_examples::galton;
using namespace std;

int main(int argc, char * argv[]) {
	for(unsigned n_bins : {7, 19, 31/*, 101*/}) {
		for(unsigned n_balls = 1000; n_balls <= 1000000; n_balls*=10) {
			string filename = "galton_" + to_string(n_bins) + "_" + to_string(n_balls);
			cout << "Simulating Galton Board with " << n_bins << " bins and " << n_balls << " balls..." << endl;

			shared_ptr<GaltonBoard> galton = GaltonBoard::create(n_bins-1, 100, 0.50, "GaltonBoard");

			Simulator sim(galton);

			sim.set_termination_time(n_balls/100);

			sim.set_checkpoint_name("galton");
			sim.set_checkpoint_interval(0);

			sim.set_gvt_interval(5);

			sim.set_classic_devs();

			// sim.set_verbose();
			sim.set_verbose(filename+".txt");

			sim.Simulate();
	
			string dist_csv = galton->getDistributionAsCSV();
			ofstream dist_stream(filename+".csv");
			dist_stream << dist_csv << endl;
			dist_stream.close();

			string norm_dist_csv = galton->getNormalizedDistributionAsCSV();
			ofstream dist_norm_stream(filename+"_normalized.csv");
			dist_norm_stream << norm_dist_csv << endl;
			dist_norm_stream.close();
		}
	}

	return 0;
}
