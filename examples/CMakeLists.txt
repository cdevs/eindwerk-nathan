# Pull in the project includes
include_directories(${PROJECT_SOURCE_DIR}/src)

# Put the resulting libraries in the `example` directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/examples)

set(EXAMPLESOURCES
   CACHE INTERNAL "example sources"
)

#=====================================================================#
# Subdirectories                                                      #
#=====================================================================#
add_subdirectory(atomicmodel_test)
add_subdirectory(trafficlight_classic)
add_subdirectory(trafficlight_parallel)
add_subdirectory(traffic)
add_subdirectory(economy)
add_subdirectory(galton)