#ifndef TRACERCUSTOM_H_
#define TRACERCUSTOM_H_

#include <fstream>
#include <string>
#include <vector>
#include <mutex>
#include <condition_variable>

#include "tracers/Tracer.h"
#include "Controller.h"

namespace cdevs {

class TracerCustom: public Tracer
{
public:
	TracerCustom(std::string filename="");
	virtual ~TracerCustom();

	void TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t);
	void TraceInternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceExternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS);
	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text);
private:
	DevsTime prevtime_;
};

} /* namespace cdevs */
#endif /* TRACERVERBOSE_H_ */
