/*
 * model.h
 *
 *  Created on: 7-mei-2015
 *      Author: david
 */

#ifndef EXAMPLES_TRAFFICLIGHT_CLASSIC_MODEL_H_
#define EXAMPLES_TRAFFICLIGHT_CLASSIC_MODEL_H_

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/polymorphic.hpp>

namespace cdevs_examples
{
namespace parallel
{
enum Modes
{
	kTrafficLightMode = 0,
	kPolicemanMode
};

class TrafficLightMode: public cdevs::State<TrafficLightMode>, public cdevs::EventCRTP<TrafficLightMode, kTrafficLightMode>
{
public:
	TrafficLightMode(std::string value = "red");
	std::string string() const;
	std::string toXML() const;

	std::string getValue() const;
	void setValue(std::string value);
private:
	std::string value_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<TrafficLightMode> >(this));
	}
};

class TrafficLight: public cdevs::Atomic<TrafficLight, TrafficLightMode>
{
public:
	TrafficLight(std::string name);

	TrafficLightMode const& ExtTransition(std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<cdevs::Port> > > inputs);

	TrafficLightMode const& IntTransition();

	std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();

	double TimeAdvance();

	std::weak_ptr<cdevs::Port> INTERRUPT;
	std::weak_ptr<cdevs::Port> OBSERVED;
private:
	TrafficLightMode state_red_ = TrafficLightMode("red");
	TrafficLightMode state_green_ = TrafficLightMode("green");
	TrafficLightMode state_yellow_ = TrafficLightMode("yellow");
	TrafficLightMode state_grey_ = TrafficLightMode("grey");
	TrafficLightMode state_manual_ = TrafficLightMode("manual");

	TrafficLight() : TrafficLight("") {};

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<TrafficLight, TrafficLightMode>>(this), INTERRUPT, OBSERVED, state_red_, state_green_, state_yellow_, state_manual_);
	}
};

class PolicemanMode : public cdevs::State<PolicemanMode>, public cdevs::EventCRTP<PolicemanMode, kPolicemanMode>
{
public:
	PolicemanMode(std::string value = "idle");
	std::string string() const;
	std::string toXML() const;

	std::string getValue() const;
	void setValue(std::string value);

private:
	std::string value_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<PolicemanMode> >(this));
	}
};

class Policeman : public cdevs::Atomic<Policeman, PolicemanMode>
{
public:
	Policeman(std::string name);

	PolicemanMode const& IntTransition();

	std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();

	double TimeAdvance();

	std::weak_ptr<cdevs::Port> OUT;
private:
	PolicemanMode state_idle_ = PolicemanMode("idle");
	PolicemanMode state_working_ = PolicemanMode("working");
	PolicemanMode state_toManual_ = PolicemanMode("toManual");
	PolicemanMode state_toAutonomous_ = PolicemanMode("toAutonomous");

	Policeman() : Policeman("") {};

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Policeman, PolicemanMode>>(this), OUT, state_idle_, state_working_);
	}
};

class TrafficSystem: public cdevs::Coupled<TrafficSystem>
{
public:
	TrafficSystem(std::string name);

	std::shared_ptr<Policeman> getPoliceMan() const;
	std::shared_ptr<TrafficLight> getTrafficLight() const;
private:
	std::shared_ptr<Policeman> policeman;
	std::shared_ptr<TrafficLight> trafficlight;

	TrafficSystem() : TrafficSystem("") {};

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Coupled<TrafficSystem>>(this), policeman, trafficlight);
		SetComponentSet({policeman, trafficlight});
	}
};

inline std::string TrafficLightMode::getValue() const
{
	return value_;
}
inline void TrafficLightMode::setValue(std::string value)
{
	value_ = value;
}

inline std::string TrafficLightMode::string() const {
	return value_;
}

inline std::string TrafficLightMode::toXML() const {
	return "<color>"+value_+"</color>\n";
}

inline std::string PolicemanMode::getValue() const
{
	return value_;
}

inline void PolicemanMode::setValue(std::string value)
{
	value_ = value;
}

inline std::string PolicemanMode::string() const {
	return value_;
}

inline std::string PolicemanMode::toXML() const {
	return "<state>"+value_+"</state>\n";
}

}
}

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs_examples::parallel::TrafficSystem)
CEREAL_REGISTER_TYPE(cdevs_examples::parallel::TrafficLightMode)
CEREAL_REGISTER_TYPE(cdevs_examples::parallel::TrafficLight)
CEREAL_REGISTER_TYPE(cdevs_examples::parallel::PolicemanMode)
CEREAL_REGISTER_TYPE(cdevs_examples::parallel::Policeman)

#endif /* EXAMPLES_TRAFFICLIGHT_CLASSIC_MODEL_H_ */
