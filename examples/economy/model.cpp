/*
 * model.cpp
 *
 *  Created on: Jun 14, 2015
 *      Author: Nathan
 *
 *
 *      The idea here is to simulate an economy of Consumers and producers.
 *      Producers will produce X amount of Gold/Silver/Bronze at a rate,
 *      Consumers will buy X amount of Gold/Silver/Bronze.
 *
 *      Prices in the Economy will vary based on Supply vs Demand.
 */

#include "model.h"
namespace cdevs_examples{


std::string cdevs_examples::ConsumerMode::string() const
{
}

std::string cdevs_examples::ConsumerMode::toXML() const
{
}

std::string cdevs_examples::ConsumerMode::getValue() const
{
}

void cdevs_examples::ConsumerMode::setValue(std::string value)
{
}

cdevs_examples::ConsumerMode::ConsumerMode(std::string value) : value_(value)
{
}



cdevs_examples::Consumer::Consumer(std::string name) : Atomic(name)
{
}

const ConsumerMode& cdevs_examples::Consumer::ExtTransition(
        std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
                std::owner_less<std::weak_ptr<cdevs::Port> > > inputs)
{
}

const ConsumerMode& cdevs_examples::Consumer::IntTransition()
{
}

std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
        std::owner_less<std::weak_ptr<cdevs::Port> > > cdevs_examples::Consumer::OutputFunction()
{
}

double cdevs_examples::Consumer::TimeAdvance()
{
}

cdevs_examples::ProducerMode::ProducerMode(std::string value) : value_(value)
{
}

std::string cdevs_examples::ProducerMode::string() const
{
}

std::string cdevs_examples::ProducerMode::toXML() const
{
}

std::string cdevs_examples::ProducerMode::getValue() const
{
}

void cdevs_examples::ProducerMode::setValue(std::string value)
{
}

cdevs_examples::Producer::Producer(std::string name) : Atomic(name)
{
}

const ProducerMode& cdevs_examples::Producer::IntTransition()
{
}

std::map<std::weak_ptr<cdevs::Port>, std::list<std::shared_ptr<const cdevs::EventBase> >,
        std::owner_less<std::weak_ptr<cdevs::Port> > > cdevs_examples::Producer::OutputFunction()
{
}

double cdevs_examples::Producer::TimeAdvance()
{
}

cdevs_examples::Economy::Economy(std::string name) : Coupled(name)
{
}

std::shared_ptr<cdevs::BaseDevs> cdevs_examples::Economy::Select(std::list<std::shared_ptr<cdevs::BaseDevs> > imm_children) const
{
}

std::shared_ptr<Producer> cdevs_examples::Economy::getProducer() const
{
}

std::shared_ptr<Consumer> cdevs_examples::Economy::getConsumer() const
{
}

void Economy::AddResourcePrice(int index, int amount)
{
	this->resource_prices_[index] += amount;
}
void Economy::AddResources(int index, int amount)
{
	this->total_resources_[index] += amount;
}


}


