
__  Current Time: 0.000000 __________________________________________ 


	INITIAL CONDITIONS in model <trafficSystem.policeman>
		Initial State: idle
		Next scheduled internal transition at time 200.000000

	INITIAL CONDITIONS in model <trafficSystem.trafficlight>
		Initial State: red
		Next scheduled internal transition at time 58.500000

__  Current Time: 58.500000 __________________________________________ 


	INTERNAL TRANSITION in model <trafficSystem.trafficlight>
		New State: green
		Output Port Configuration:
			port <OBSERVED>:
				grey
		Next scheduled internal transition at time 108.500000

__  Current Time: 108.500000 __________________________________________ 


	INTERNAL TRANSITION in model <trafficSystem.trafficlight>
		New State: yellow
		Output Port Configuration:
			port <OBSERVED>:
				yellow
		Next scheduled internal transition at time 118.500000

__  Current Time: 118.500000 __________________________________________ 


	INTERNAL TRANSITION in model <trafficSystem.trafficlight>
		New State: red
		Output Port Configuration:
			port <OBSERVED>:
				grey
		Next scheduled internal transition at time 178.500000

__  Current Time: 178.500000 __________________________________________ 


	INTERNAL TRANSITION in model <trafficSystem.trafficlight>
		New State: green
		Output Port Configuration:
			port <OBSERVED>:
				grey
		Next scheduled internal transition at time 228.500000

__  Current Time: 200.000000 __________________________________________ 


	INTERNAL TRANSITION in model <trafficSystem.policeman>
		New State: working
		Output Port Configuration:
			port <OUT>:
				toManual
		Next scheduled internal transition at time 300.000000

	EXTERNAL TRANSITION in model <trafficSystem.trafficlight>
		Input Port Configuration:
			port <INTERRUPT>: 
				toManual
		New State: manual
		Next scheduled internal transition at time inf

__  Current Time: 300.000000 __________________________________________ 


	INTERNAL TRANSITION in model <trafficSystem.policeman>
		New State: idle
		Output Port Configuration:
			port <OUT>:
				toAutonomous
		Next scheduled internal transition at time 500.000000

	EXTERNAL TRANSITION in model <trafficSystem.trafficlight>
		Input Port Configuration:
			port <INTERRUPT>: 
				toAutonomous
		New State: red
		Next scheduled internal transition at time 360.000000

__  Current Time: 360.000000 __________________________________________ 


	INTERNAL TRANSITION in model <trafficSystem.trafficlight>
		New State: green
		Output Port Configuration:
			port <OBSERVED>:
				grey
		Next scheduled internal transition at time 410.000000
