/*
 * RuntimeDevsException.h
 *
 *  Created on: May 5, 2015
 *      Author: paul
 */

#ifndef RUNTIMEDEVSEXCEPTION_H_
#define RUNTIMEDEVSEXCEPTION_H_

#include <stdexcept>

namespace cdevs {

class RuntimeDevsException: public std::runtime_error
{
public:
	using std::runtime_error::runtime_error;
};

} /* namespace cdevs */

#endif /* RUNTIMEDEVSEXCEPTION_H_ */
