#include "../tracers/TracerJson.h"
#include "../AtomicDevs.h"
#include "../../examples/atomicmodel_test/model.h"
#include "../exceptions/LogicDevsException.h"
#include <iostream>
#include "gtest/gtest.h"
#include "../Controller.h"
#include "atomicmodel_test/model.h"

namespace cdevs {

class TracerJsonTest: public ::testing::Test
{
protected:
	void Constructors()
	{
		TracerJson* t1 = new TracerJson(controller_,  "out.json");
		EXPECT_EQ(t1->filename_, "out.json");
		EXPECT_FALSE(t1->stream_);
		delete t1;

		TracerJson* t2 = new TracerJson(controller_);
		EXPECT_EQ(t2->filename_, "");
		EXPECT_FALSE(t2->stream_);
		delete t2;
	}

	void StartStop()
	{
		TracerJson* t = new TracerJson(controller_, "out.json");
		EXPECT_FALSE(t->stream_);
		EXPECT_THROW(t->PrintString("Error"), LogicDevsException);
		t->StartTracer(false);
		EXPECT_TRUE(t->stream_);
		t->StopTracer();
		EXPECT_FALSE(t->stream_);
		std::ifstream o("out.json");
		std::stringstream out;
		out << o.rdbuf();
		EXPECT_EQ("{\n\"trace\":{\n\n]\n}\n}", out.str());
		delete t;
		remove("out.json");

		t = new TracerJson(controller_, "");
		//Redirect cout
		std::stringbuf* my_cout = new std::stringbuf();
		std::streambuf *old = std::cout.rdbuf(my_cout);
		EXPECT_FALSE(t->stream_);
		t->StartTracer(false);
		EXPECT_EQ(&std::cout, t->stream_);
		t->StopTracer();
		//Restore cout
		std::cout.rdbuf(old);
		EXPECT_EQ("{\n\"trace\":{\n\n]\n}\n}", my_cout->str());
		EXPECT_FALSE(t->stream_);
		delete my_cout;
		delete t;
	}

	void Print()
	{
		TracerJson* t = new TracerJson(controller_, "out.json");
		t->StartTracer(false);
		t->PrintString("This is a test");
		t->StopTracer();
		std::ifstream o("out.json");
		std::stringstream out;
		out << o.rdbuf();
		EXPECT_EQ("{\n\"trace\":{\nThis is a test\n]\n}\n}", out.str());
		EXPECT_THROW(t->PrintString("Error!"), LogicDevsException);
		remove("out.Json");
		delete t;
	}

	void TraceInitialize()
	{
		TracerJson* t = new TracerJson(controller_, "out.json");
		std::string modelpath = "examples/libAtomicModelTest";
		std::shared_ptr<AtomicDevs> model = cdevs_examples::atomic::TrafficLight::create("VerkeersLicht");
		model->set_time_next(DevsTime(60, 0));
		t->StartTracer(false);
		t->TraceInitialize(model, DevsTime());
		controller_->PerformActions();
		t->StopTracer();
		std::ifstream o("out.json");
		std::stringstream out;
		out << o.rdbuf();
		std::ifstream e("expected/json/TraceInitialize.json");
		std::stringstream exp;
		exp << e.rdbuf();
		EXPECT_EQ(exp.str(), out.str());
		remove("out.json");
		delete t;
	}

	virtual void SetUp()
	{
		controller_ = Controller::create(NULL);
	}
	virtual void TearDown()
	{
	}

	std::shared_ptr<Controller> controller_;
};

TEST_F(TracerJsonTest, Constructors)
{
	Constructors();
}

TEST_F(TracerJsonTest, StartStop)
{
	StartStop();
}

TEST_F(TracerJsonTest, Print)
{
	Print();
}

TEST_F(TracerJsonTest, TraceInitialize)
{
	TraceInitialize();
}

} /* namespace ns_DEVS */
