#ifndef SRC_TRACERS_TRACERACTION_H_
#define SRC_TRACERS_TRACERACTION_H_

#include <string>
#include "Tracer.h"
#include "../AtomicDevs.h"

#include <cereal/access.hpp>
#include <cereal/types/memory.hpp>

namespace cdevs {

/**
 * \brief An object that is used to store delayed actions
 *
 * This will be used when tracing. When the tracer wants to output
 * it calls the controller. The controller makes sure that the text
 * it output at the right time. To store all these outputs all information
 * of this output, including the model, the text, the tracer that called,
 * and the time of output will be stored.
 */
class TracerAction
{
public:
	TracerAction();
	TracerAction(DevsTime time, std::shared_ptr<AtomicDevs> model, std::string text, std::weak_ptr<Tracer> tracer);
	virtual ~TracerAction();

	const std::string& getText() const;
	void setText(const std::string& text);

	std::weak_ptr<Tracer> getTracer() const;
	void setTracer(std::weak_ptr<Tracer> tracer);

	std::shared_ptr<AtomicDevs> getModel() const;
	void setModel(std::shared_ptr<AtomicDevs> model);

	const DevsTime& getTime() const;
	void setTime(const DevsTime& time);

private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(time_, model_, text_ , tracer_);
	}

	DevsTime time_;
	std::weak_ptr<AtomicDevs> model_;
	std::string text_;
	std::weak_ptr<Tracer> tracer_;
};

} /* namespace cdevs */

#endif /* SRC_TRACERS_TRACERACTION_H_ */
