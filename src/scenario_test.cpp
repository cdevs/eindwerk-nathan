#include "gtest/gtest.h"
#include "Simulator.h"
#include "trafficlight_classic/model.h"

namespace cdevs {

class ScenarioTest: public ::testing::Test
{
protected:
	void Verbose()
	{
		std::ifstream o("classic.txt");
		std::stringstream out;
		out << o.rdbuf();
		std::ifstream e("expected/verbose/classic.txt");
		std::stringstream expected;
		expected << e.rdbuf();
		EXPECT_EQ(expected.str(), out.str());
	}

	void XML()
	{
		std::ifstream o("classic.xml");
		std::stringstream out;
		out << o.rdbuf();
		std::ifstream e("expected/xml/classic.xml");
		std::stringstream expected;
		expected << e.rdbuf();
		EXPECT_EQ(expected.str(), out.str());
	}

	void JSON()
	{
		std::ifstream o("classic.json");
		std::stringstream out;
		out << o.rdbuf();
		std::ifstream e("expected/json/classic.json");
		std::stringstream expected;
		expected << e.rdbuf();
		EXPECT_EQ(expected.str(), out.str());
	}

	virtual void SetUp()
	{
		// std::string modelpath = "examples/libTrafficLight_Classic";
		// std::shared_ptr<BaseDevs> model = BaseDevs::load(modelpath);
		std::shared_ptr<cdevs_examples::classic::TrafficSystem> model =
		        cdevs_examples::classic::TrafficSystem::create("trafficSystem");

		Simulator sim(model);

		sim.set_termination_time(400.0);

		sim.set_verbose("classic.txt");
		sim.set_xml("classic.xml");
		sim.set_json("classic.json");

		sim.set_classic_devs();

		sim.Simulate();
	}
	virtual void TearDown()
	{
	}
};

TEST_F(ScenarioTest, Verbose)
{
	Verbose();
}

TEST_F(ScenarioTest, Xml)
{
	XML();
}

TEST_F(ScenarioTest, Json)
{
	JSON();
}

}
