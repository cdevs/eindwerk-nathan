/*
 * Message_test.cpp
 *
 *  Created on: Jun 13, 2015
 *      Author: paul
 */

#include "Message.h"
#include "gtest/gtest.h"
#include "DevsTime.h"
#include <map>

namespace cdevs {

TEST(Message_Test, Operators)
{
	DevsTime time1(100,0), time2(42, 1);
	Message m1(time1), m2(time2);
	EXPECT_TRUE(m1 > m2);
	EXPECT_FALSE(m1 == m2);
	EXPECT_FALSE(m2 > m1);
}

TEST(Message_Test, GetSetTest)
{
	DevsTime time1(100,0), time2(42, 1);
	Message m1(time1), m2(time2);
	m1.set_color(Color::kRed1);
	EXPECT_EQ(m1.get_color(), Color::kRed1);
}

}


