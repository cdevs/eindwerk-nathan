/*
 * BaseDEVS.h
 *
 *  Created on: Feb 23, 2015
 *      Author: paul
 */

#ifndef BASEDEVS_H_
#define BASEDEVS_H_

#include <string>
#include <vector>
#include <list>
#include <map>
#include <memory>

#include "Port.h"
#include "utility.h"
#include "Predefines.h"

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/vector.hpp>

/**
 * Load the correct header files for the used compiler
 */
#if defined(PREDEF_COMPILER_VISUALC) // Microsoft compiler
#include <windows.h>
#elif defined(PREDEF_COMPILER_GCC) // GNU compiler or Mac OS X
#include <dlfcn.h>
#else
#error Your compiler is not supported
#endif

namespace cdevs {

class Simulator;
class AtomicDevs;
class CoupledDevs;

struct DevsSnapshot
{
	DevsTime time_last_;
	DevsTime time_next_;
	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > input_;
	double elapsed_;
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(time_last_, time_next_, input_, elapsed_);
	}
};

/**
 Abstract base class for AtomicDEVS and CoupledDEVS classes.

 This class provides basic DEVS attributes and query/set methods.
 */
class BaseDevs
{
public:
	BaseDevs(std::string name);
	virtual ~BaseDevs();

	bool ModelTransition(StateBase const&);

	void set_parent(const std::weak_ptr<BaseDevs>& parent);

	const std::weak_ptr<BaseDevs> get_parent() const;

	std::weak_ptr<Port> AddPort(std::string, bool);
	void RemovePort(std::weak_ptr<Port>);
	std::weak_ptr<Port> AddInPort(std::string);
	std::weak_ptr<Port> AddOutPort(std::string);

	const virtual std::string get_model_name();
	const std::string get_model_full_name();

	void set_model_id(int id);
	int get_model_id() const;

	DevsTime get_time_last() const;
	void set_time_last(const DevsTime& timeLast);
	DevsTime get_time_next() const;
	void set_time_next(const DevsTime& timeNext);
	bool operator==(const BaseDevs& rhs);

	void set_time_elapsed(double timeElapsed);

	void set_handle(void* h);
	void* get_handle() const;

	static std::shared_ptr<BaseDevs> Load(std::string path);

	const std::vector<std::shared_ptr<Port>>& get_input_ports();

	const std::vector<std::shared_ptr<Port>>& get_output_ports();

	double get_time_elapsed();

	virtual void FlattenConnections();
	virtual void UnflattenConnections();
	virtual void set_location(int location, bool force) = 0;

	int get_location() const;
	virtual int Finalize(std::string name, int model_counter, std::map<int, std::shared_ptr<BaseDevs>>& model_ids,
	        std::list<std::shared_ptr<BaseDevs>> select_hierarchy) = 0;

	bool isCoupled() const;
	const std::string& get_full_name() const;

	void set_full_name(const std::string& fullName);

	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > my_output_;
	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >,
	        std::owner_less<std::weak_ptr<Port> > > my_input_;

	static int basedevs_counter;

	virtual std::shared_ptr<BaseDevs> getPtr() = 0; // { return this->shared_from_this(); };

protected:
	bool is_coupled_;

private:
	void* handle_;

	BaseDevs()
		: BaseDevs("")
	{
	}
	;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(basedevs_counter, is_coupled_, name_, full_name_, model_id_, parent_,
		        input_ports_, output_ports_, time_last_, time_next_, location_, old_states_, num_children_,
		        elapsed_, my_input_, my_output_);
	}
protected:
	std::string name_;
	std::string full_name_;
	int model_id_;
	std::weak_ptr<BaseDevs> parent_;

	std::vector<std::shared_ptr<Port> > input_ports_;
	std::vector<std::shared_ptr<Port> > output_ports_;

	DevsTime time_last_;
	DevsTime time_next_;

	int location_;
	std::vector<DevsSnapshot> old_states_;
	unsigned num_children_;

	double elapsed_;
};

inline const std::string& BaseDevs::get_full_name() const
{
	return full_name_;
}

inline void BaseDevs::set_full_name(const std::string& fullName)
{
	full_name_ = fullName;
}
inline bool BaseDevs::isCoupled() const
{
	return is_coupled_;
}
inline int BaseDevs::get_location() const
{
	return location_;
}
inline const std::vector<std::shared_ptr<Port>>& BaseDevs::get_input_ports()
{
	return input_ports_;
}

inline const std::vector<std::shared_ptr<Port>>& BaseDevs::get_output_ports()
{
	return output_ports_;
}
inline const std::weak_ptr<BaseDevs> BaseDevs::get_parent() const
{
	return parent_;
}

template<class Base, class Derived>
class DevsCRTP: public Base, public std::enable_shared_from_this<Derived>
{
public:
	using Base::Base;

	std::shared_ptr<BaseDevs> getPtr()
	{
		return std::enable_shared_from_this<Derived>::shared_from_this();
	}

	/////////////////////////////////////////////////////////
	// CRTP style model factory methods
	/////////////////////////////////////////////////////////
	template<typename ... T>
	static std::shared_ptr<Derived> create(T&& ... all)
	{
		std::shared_ptr<Derived> model = std::shared_ptr<Derived>(new Derived(std::forward<T>(all)...));
		std::shared_ptr<BaseDevs> m = model->getPtr(); // std::static_pointer_cast<BaseDevs>(model);
		for (auto& p : m->get_input_ports())
			p->set_host_devs(m);
		for (auto& p : m->get_output_ports())
			p->set_host_devs(m);

		return model;
	}
private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<BaseDevs>(this));
	}
};

} /* namespace ns_DEVS */

#endif /* BASEDEVS_H_ */
